Built and tested with:
- Python 3.8
- simplekml (1.3.6)
- geopandas (0.9.0)

I have been using Anaconda python for this. Start by installing the base(root) environment. Select the latest python version for this.  Then, open the Anaconda Navigator.  That will give you a GUI for installing most packages. In the navigator create a new python3.8  “Environments” (call it python38, no space, no funny characters.) Once the python38 environment is created, then you can install, in that specific environment, Spyder and or Jupiter Notebook. If you get in trouble, you can simply delete the environment and stats over again. The navigator is usually tied to the base (root) environment. Don’t install extra packages in there.

If you have some difficulty installing Geopandas, you may need to install extra “Channels” like conda-forge.  For simplekml.py, use Pip install in the Anaconda Prompt. Type “activate python38”, no quotes.  That will bring you to the environment. Then type “pip install simplekml”, no quotes.

https://pypi.org/project/simplekml/
https://geopandas.org/getting_started/install.html

Input .shp files NEEDS 2 fields. A 'name' field and a 'description' field for the kml tags. (Download the zip folder with the test files.) The field names can be called anything in the shape file (like 'name' and 'desc').  You can then specify the exact name for both fields at the beginning to the script under the USER VARIABLES section.

NAME = 'name'  
DESCRIPTION = 'desc'

The last variable that needs to be modified using a text editor (or Spyder) is the input file directory. Don't forget to keep the 'r' in front of the quotes.
INPUT_PATH = r'C:\temp\DJI\test'

Cheers!
Nicolas Cadieux, McGill Applied Remote Sensing Lab, https://arsl.geog.mcgill.ca/


