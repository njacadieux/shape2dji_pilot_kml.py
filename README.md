# Shape2DJI_Pilot_KML.py

Warning!!!
Please go to https://gitlab.com/njacadieux/gis2dji for the new version of the software.



This python script will scan a directory, find all the .shp files, reproject to EPSG 4326, create an output directory and make a new .kml for every line or polygon found in the files.  .KML are 100% compatible with DJI PILOT.  This is not the case with .kml files created by ESRI or QGIS.


- Nicolas Cadieux, McGill Applied Remote Sensing Lab, https://arsl.geog.mcgill.ca/.
- YouTube video with instructions https://youtu.be/Wzr-zP5wG1g
